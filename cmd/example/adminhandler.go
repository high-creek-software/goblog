package main

import (
	"github.com/go-chi/chi"
	"gitlab.com/kendellfab/blog"
	"gitlab.com/kendellfab/fazer"
	"net/http"
)

type AdminHandler struct {
	*fazer.Fazer
	blog.Manager
}

// GetList returns the admin list page, or the page that displays the added posts
func (ah AdminHandler) GetList(w http.ResponseWriter, r *http.Request) {
	ah.RenderTemplate(w, r, nil, "admin.list")
}

// GetEdit returns the edit page for a given post
func (ah AdminHandler) GetEdit(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	ah.RenderTemplate(w, r, map[string]interface{}{"Id": id}, "admin.edit")
}
