{{define "Content"}}
    <div class="section" id="editPost">
        <div class="container">
            <div class="level">
                <div class="level-left">
                    <div class="level-item">
                        <h1 class="is-size-4">Edit Post</h1>
                    </div>
                </div>
                <div class="level-right">
                </div>
            </div>
            <div class="columns">
                <div class="column is-three-quarters">
                    <div class="box">
                        <div class="field">
                            <div class="control">
                                <input class="input" type="text" v-model="post.title" />
                            </div>
                        </div>
                        <p class="has-padding"><a :href="'/blog/'+post.slug">[[post.title]]</a></p>

                        <div class="field">
                            <div class="control">
                                <textarea class="textarea" v-model="post.body" rows="15"></textarea>
                            </div>
                        </div>

                        <div class="file">
                            <label class="file-label">
                                <input class="file-input" type="file" name="file" @change="imageChanged('post', $event.target.name, $event.target.files);" />
                                <span class="file-cta">
                                  <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                  </span>
                                  <span class="file-label">
                                    Add image to post...
                                  </span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="box">
                        <h2 class="is-size-5">Post Description</h2>
                        <div class="field">
                            <div class="control">
                                <textarea class="textarea" v-model="post.description" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box">
                        <h2 class="is-size-4">Publish</h2>
                        <hr />
                        <div class="level is-mobile">
                            <div class="level-item">
                                <button class="button is-primary" v-on:click="updatePost();">Update</button>
                            </div>
                            <div class="level-item">
                                <label class="checkbox drop-top">
                                    <input type="checkbox" v-model="post.active" @change="toggleActive()">
                                    Active
                                </label>
                            </div>
                            <div class="level-item">
                                <label class="checkbox drop-top">
                                    <input type="checkbox" v-model="post.featured" @change="toggleFeatured()">
                                    Featured
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <h2 class="is-size-4">Cover Image</h2>
                        <hr />
                        <div v-if="post.cover === ''">
                            <div class="file">
                                <label class="file-label">
                                    <input class="file-input" type="file" name="file" @change="imageChanged('cover', $event.target.name, $event.target.files);" />
                                    <span class="file-cta">
                                  <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                  </span>
                                  <span class="file-label">
                                    Add cover image...
                                  </span>
                                </span>
                                </label>
                            </div>
                        </div>
                        <div v-if="post.cover !== ''">
                            <img :src="post.cover" />
                        </div>
                        <div class="level drop-top is-mobile">
                            <div class="level-left">
                                <div class="level-item">
                                    <a v-on:click="deleteImage()" v-if="post.cover !== ''">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                                <div class="level-item">
                                    <a v-on:click="cancelDelete();" v-if="post.cover === '' && imageScratch !== ''">
                                        <i class="fas fa-undo"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <h2 class="is-size-4">Tags</h2>
                        <hr />
                        <div class="select">
                            <select v-model="tag" @change="addTagToPost();">
                                <option v-for="t in tags" v-bind:value="t.id">[[t.title]]</option>
                            </select>
                        </div>
                        <hr />
                        <table class="table is-striped is-fullwidth">
                            <thead>
                            <tr>
                                <th>Tag</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="t in post.tags">
                                <td>[[t.title]]</td>
                                <td><a href="#" v-on:click="deleteTag(t);">Delete</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{end}}

{{define "Js"}}
<script>
    var postId = "{{.Id}}";
    new Vue({
        el: "#editPost",
        data: {
            post: {},
            imageScratch: "",
            busy: false,
            tags: [],
            tag: {}
        },
        methods: {
            updatePost: function() {
                var self = this;
                axios.post("/api/blog/post/"+postId, self.post).then(function(response) {
                    console.log(response.data);
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                });
            },
            imageChanged: function(src, name, files) {
                if(!files.length) {
                    return;
                }

                var form = new FormData();
                form.append(name, files[0], files[0].name);

                var url = "/uploads/blog";
                var self = this;
                axios.post(url, form).then(function(response) {
                    if(src === 'cover') {
                        self.post.cover = response.data.path;
                        self.updatePost();
                    } else if(src === 'post') {
                        self.post.body = self.post.body + "![](" + response.data.path + ")";
                        self.updatePost();
                    }
                })
            },
            deleteImage: function() {
                this.imageScratch = this.post.cover;
                this.post.cover = '';
            },
            cancelDelete: function() {
                this.post.cover = this.imageScratch;
            },
            toggleActive: function () {
                var self = this;
                axios.post("/api/blog/post/"+postId+ "/active").then(function(response) {

                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            toggleFeatured: function() {
                var self = this;
                axios.post("/api/blog/post/"+postId+ "/featured").then(function(response) {

                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            addTagToPost: function() {
                var self = this;
                axios.put("/api/blog/post/" + postId + "/tag/" + self.tag).then(function(response) {
                    var arr = self.post.tags;
                    for(var i = 0; i < self.tags.length; i++) {
                        if(self.tags[i].id === self.tag) {
                            arr.push(self.tags[i]);
                        }
                    }
                    self.post.tags = arr;
                }).catch(function(error) {
                    alert(JSON.stringify(error));
                })
            },
            deleteTag: function(tag) {
                var self = this;
                axios.delete("/api/blog/post/"+postId+"/tag/"+tag.id).then(function(response) {
                    var index = -1;
                    for(var i = 0; i < self.post.tags.length; i++) {
                        if(tag.id === self.post.tags[i].id) {
                            index = i;
                        }
                    }

                    var arr = self.post.tags;
                    arr.splice(index, 1);
                    self.post.tags = arr;
                }).catch(function(error) {
                    alert(JSON.stringify(error))
                });
            }
        },
        mounted: function() {
            var self = this;
            axios.get("/api/blog/post/"+postId).then(function(response) {
                self.post = response.data;
            }).catch(function(error) {
                alert(JSON.stringify(error));
            });

            axios.get("/api/blog/tag").then(function(response) {
                self.tags = response.data;
            }).catch(function(error) {
                alert(JSON.stringify(error));
            });
        }
    })
</script>
{{end}}