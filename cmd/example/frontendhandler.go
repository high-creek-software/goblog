package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/kendellfab/blog"
	"gitlab.com/kendellfab/fazer"
)

// FrontendHandler houses the routes for displaying the post list and the actual post
type FrontendHandler struct {
	*fazer.Fazer
	blog.Manager
}

// GetIndex returns the index page that acts as the posts list
func (fh FrontendHandler) GetIndex(w http.ResponseWriter, r *http.Request) {
	limit := queryInt(r, "limit", 5)
	page := queryInt(r, "page", 1)

	data := make(map[string]interface{})
	posts, pg, err := fh.PageActivePosts(limit, page)
	if err == nil {
		data["Posts"] = posts
		data["Paginator"] = pg
	} else {
		log.Println("error loading posts:", err)
	}

	fh.RenderTemplate(w, r, data, "front.index")
}

// GetPost returns the post page, with a post or error
func (fh FrontendHandler) GetPost(w http.ResponseWriter, r *http.Request) {
	slug := chi.URLParam(r, "slug")
	post, err := fh.GetPostWithSlug(slug)
	data := map[string]interface{}{"Post": post, "Error": err}
	fh.RenderTemplate(w, r, data, "front.post")
}
