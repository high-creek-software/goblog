package goblog

import (
	"encoding/json"
	"io"
	"log"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Manager struct {
	orm *gorm.DB
}

func NewManager(orm *gorm.DB) Manager {
	err := orm.AutoMigrate(&Post{}, &Tag{})
	if err != nil {
		log.Fatal("Error migrating post and tag", err)
	}
	return Manager{orm: orm}
}

func (m Manager) GetPost(pid string) (Post, error) {
	var p Post
	err := m.orm.Preload("Tags").Find(&p, Post{ID: pid}).Error
	if err != nil {
		return p, err
	}
	return p, nil
}

func (m Manager) GetPostWithSlug(slug string) (Post, error) {
	var p Post
	err := m.orm.Preload("Tags").Find(&p, Post{Slug: slug}).Error
	return p, err
}

func (m Manager) ListPosts(limit, offset int) ([]Post, error) {
	var posts []Post
	// Select("id", "title", "slug", "description").
	err := m.orm.Preload("Tags").Order("created_at desc").Limit(limit).Offset(offset).Find(&posts).Error
	return posts, err
}

func (m Manager) ListActivePosts(limit, offset int) ([]Post, error) {
	var posts []Post
	err := m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true}).Limit(limit).Offset(offset).Find(&posts).Error
	return posts, err
}

func (m Manager) ListActivePostSummaries() ([]PostSummary, error) {
	var summaries []PostSummary
	err := m.orm.Model(&Post{}).Order("updated_at desc").Where(&Post{Active: true}).Find(&summaries).Error
	return summaries, err
}

func (m Manager) PageActivePosts(limit, page int) ([]Post, Paginator, error) {
	pg := Paginator{}
	var posts []Post

	var count int64
	err := m.orm.Model(&Post{}).Where("active = ?", true).Count(&count).Error

	if err != nil {
		return posts, pg, err
	}

	offset := 0
	if page >= 1 {
		offset = (page - 1) * limit
	}

	err = m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true}).Limit(limit).Offset(offset).Find(&posts).Error
	if err != nil {
		return posts, pg, err
	}

	observedOffset := int64(limit * page)
	if len(posts) == limit && observedOffset < count {
		pg.HasMore = true
	}
	pg.MorePage = page + 1

	if page > 1 {
		pg.HasLess = true
	}
	pg.LessPage = page - 1

	return posts, pg, nil
}

func (m Manager) ListFeaturedPosts() ([]Post, error) {
	var posts []Post
	err := m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true, Featured: true}).Find(&posts).Error
	return posts, err
}

func (m Manager) ListActiveFeaturedPosts() ([]Post, error) {
	var posts []Post
	err := m.orm.Preload("Tags").Order("updated_at desc").Where(&Post{Active: true, Featured: true}).Find(&posts).Error
	return posts, err
}

func (m Manager) ListSlugUpdatedActive() ([]Post, error) {
	var posts []Post
	err := m.orm.Select([]string{"slug", "updated_at"}).Order("updated_at desc").Where(&Post{Active: true}).Find(&posts).Error
	return posts, err
}

func (m Manager) NewPost(r io.Reader) (Post, error) {
	var p Post
	err := json.NewDecoder(r).Decode(&p)
	if err != nil {
		return p, err
	}
	p.ID = uuid.New().String()
	p.Slug = URLSafeSlug(p.Title)
	err = m.orm.Create(&p).Error
	return p, err
}

func (m Manager) NewTag(r io.Reader) (Tag, error) {
	var t Tag
	err := json.NewDecoder(r).Decode(&t)
	if err != nil {
		return t, err
	}
	t.ID = uuid.New().String()
	t.Slug = URLSafeSlug(t.Title)
	err = m.orm.Create(&t).Error
	return t, err
}

func (m Manager) UpdatePost(r io.Reader) error {
	var p Post
	err := json.NewDecoder(r).Decode(&p)
	if err != nil {
		return err
	}
	p.Slug = URLSafeSlug(p.Title)
	return m.orm.Model(&p).Save(p).Error
}

func (m Manager) UpdateTag(r io.Reader) error {
	var t Tag
	err := json.NewDecoder(r).Decode(&t)
	if err != nil {
		return err
	}
	t.Slug = URLSafeSlug(t.Title)
	return m.orm.Model(&t).Save(t).Error
}

func (m Manager) ToggleActive(pid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Update("active", !p.Active).Error
}

func (m Manager) ToggleFeatured(pid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Update("featured", !p.Featured).Error
}

func (m Manager) ListTags() ([]Tag, error) {
	var tags []Tag
	err := m.orm.Find(&tags).Error
	return tags, err
}

func (m Manager) GetTagWithPosts(tid string, limit, offset int) (Tag, error) {
	var t Tag
	err := m.orm.Find(&t, Tag{ID: tid}).Error
	if err != nil {
		return t, err
	}

	var posts []*Post
	err = m.orm.Model(&t).Limit(limit).Offset(offset).Association("Posts").Find(&posts)
	if err != nil {
		return t, err
	}
	t.Posts = posts
	return t, nil
}

func (m Manager) GetTagWithPostsBySlug(slug string, limit, page int) (Tag, Paginator, error) {
	pg := Paginator{}
	var t Tag
	err := m.orm.Find(&t, Tag{Slug: slug}).Error
	if err != nil {
		return t, pg, err
	}

	offset := 0
	if page >= 1 {
		offset = (page - 1) * limit
	}

	var posts []*Post
	err = m.orm.Model(&t).Where("active = ?", true).Limit(limit).Offset(offset).Association("Posts").Find(&posts)
	if err != nil {
		return t, pg, err
	}
	t.Posts = posts

	if len(posts) == limit {
		pg.HasMore = true
	}
	pg.MorePage = page + 1

	if page > 1 {
		pg.HasLess = true
	}
	pg.LessPage = page - 1

	return t, pg, nil
}

func (m Manager) AddTagToPost(pid, tid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	var t Tag
	err = m.orm.Find(&t, Tag{ID: tid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Association("Tags").Append(&t)
}

func (m Manager) RemoveTagFromPost(pid, tid string) error {
	var p Post
	err := m.orm.Find(&p, Post{ID: pid}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&p).Association("Tags").Delete(&Tag{ID: tid})
}
