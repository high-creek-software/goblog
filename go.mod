module gitlab.com/high-creek-software/goblog

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/uuid v1.1.2
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.5
)
