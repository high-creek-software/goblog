package goblog

import "time"

type PostSummary struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	Title     string
	Slug      string
	Teaser    string
	Featured  bool
	Active    bool
	Cover     string
	CoverAlt  string
}
