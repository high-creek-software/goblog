package goblog

import (
	"time"
)

type Tag struct {
	ID          string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:",omitempty"`
	Title       string     `json:"title"`
	Slug        string     `json:"slug" gorm:"index:idx_tag_slug"`
	Body        string     `json:"body"`
	Posts       []*Post    `gorm:"many2many:post_tags;"`
	Cover       string     `json:"cover" gorm:"type:varchar(2048)"`
	CoverAlt    string     `json:"coverAlt" gorm:"type:text"`
	CoverCredit string     `json:"coverCredit" gorm:"type:varchar(4096)"`
}
