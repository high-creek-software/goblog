package goblog

import (
	"net/url"
	"strings"
)

func URLSafeSlug(input string) string {
	return url.QueryEscape(strings.ToLower(strings.Replace(input, " ", "-", -1)))
}
