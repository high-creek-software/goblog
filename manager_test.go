package goblog

import (
	"bytes"
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestSaveTag(t *testing.T) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		t.Fatal("Error opening database", err)
	}

	manager := NewManager(db)

	tagData := `{"title": "My Tag"}`
	r := bytes.NewBufferString(tagData)

	tag, tErr := manager.NewTag(r)
	if tErr != nil {
		t.Fatal("Error storing tag", tErr)
	}

	if tag.Slug != "my-tag" {
		t.Fatalf("Error slug expected: my-tag; got %s", tag.Slug)
	}
}

func TestSavePost(t *testing.T) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		t.Fatal("Error opening database", err)
	}

	manager := NewManager(db)

	post1Data := `{"title": "Post 1"}`
	r := bytes.NewBufferString(post1Data)
	post1, err := manager.NewPost(r)
	if err != nil {
		t.Fatal("Error storing post 1", err)
	}

	if post1.Slug != "post-1" {
		t.Fatalf("Error slug expected: post-1; got %s", post1.Slug)
	}
}

func TestTagWithPosts(t *testing.T) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		t.Fatal("Error opening database", err)
	}

	manager := NewManager(db)

	tagData := `{"title": "My Tag"}`
	r := bytes.NewBufferString(tagData)

	tag, tErr := manager.NewTag(r)
	if tErr != nil {
		t.Fatal("Error storing tag", tErr)
	}

	post1Data := `{"title": "Post 1"}`
	post2Data := `{"title": "Post 2"}`
	post3Data := `{"title": "Post 3"}`
	post4Data := `{"title": "Post 4"}`

	r = bytes.NewBufferString(post1Data)
	post1, err := manager.NewPost(r)
	if err != nil {
		t.Fatal("Error storing post 1", err)
	}

	r = bytes.NewBufferString(post2Data)
	post2, err := manager.NewPost(r)
	if err != nil {
		t.Fatal("Error storing post 2", err)
	}

	r = bytes.NewBufferString(post3Data)
	post3, err := manager.NewPost(r)
	if err != nil {
		t.Fatal("Error storing post 3", err)
	}

	r = bytes.NewBufferString(post4Data)
	post4, err := manager.NewPost(r)
	if err != nil {
		t.Fatal("Error storing post 4", err)
	}

	if err := manager.AddTagToPost(post1.ID, tag.ID); err != nil {
		t.Fatal("Error adding tag to post 1", err)
	}
	if err := manager.AddTagToPost(post2.ID, tag.ID); err != nil {
		t.Fatal("Error adding tag to post 2", err)
	}
	if err := manager.AddTagToPost(post3.ID, tag.ID); err != nil {
		t.Fatal("Error adding tag to post 3", err)
	}
	if err := manager.AddTagToPost(post4.ID, tag.ID); err != nil {
		t.Fatal("Error adding tag to post 4", err)
	}

	tag, tErr = manager.GetTagWithPosts(tag.ID, 10, 0)
	if len(tag.Posts) != 4 {
		t.Errorf("Tag post length got: %d; want 4", len(tag.Posts))
	}

	tag, tErr = manager.GetTagWithPosts(tag.ID, 2, 0)
	if len(tag.Posts) != 2 {
		t.Errorf("Tag post length got: %d; want 2", len(tag.Posts))
	}

}
